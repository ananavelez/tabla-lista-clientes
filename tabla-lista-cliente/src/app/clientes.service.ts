import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ListaClientes } from './lista-clientes'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  constructor(
    private http: HttpClient
  ) { }

  getClientes():Observable<ListaClientes[]>{
    return this.http.get<ListaClientes[]>('http://localhost:3000/api')
  }
}
