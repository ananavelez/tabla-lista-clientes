export interface ListaClientes {
    ClienteId:string,
    Nombre:string,
    Rut:string,
    Telefono:string,
    Correo:string
}
