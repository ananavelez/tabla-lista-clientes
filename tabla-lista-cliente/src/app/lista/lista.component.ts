import { Component, OnInit } from '@angular/core';
import { ClientesService } from '../clientes.service';
import { ListaClientes } from '../lista-clientes';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  lista:ListaClientes[]

public result:any;
  constructor(
    private client: ClientesService
  ) {}

  ngOnInit(): void {
    this.client.getClientes().subscribe(data =>
      this.lista = data      
      );
  }
}
